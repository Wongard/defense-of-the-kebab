#include "TextureManager.h"

SDL_Color color = { 255,255,255 };

SDL_Texture* TextureManager::LoadTexture(const char* fName)
{
	SDL_Surface* tmpSurface = IMG_Load(fName);
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Game::renderer, tmpSurface);
	SDL_FreeSurface(tmpSurface);

	return tex;
}
void TextureManager::Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest)
{
	SDL_RenderCopy(Game::renderer, tex, &src, &dest);
}
SDL_Texture* TextureManager::FontTexture(const char* text, const char* fName, int fSize)
{
	TTF_Font* font = TTF_OpenFont(fName, fSize);
	if (font == NULL)std::cout << "ERROR!!!(Font)" << std::endl;
	SDL_Surface* tmpSurface = TTF_RenderText_Solid(font, text, color);
	if (tmpSurface == NULL)std::cout << "ERROR!!!(tmpSurface)" << std::endl;
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Game::renderer, tmpSurface);
	SDL_FreeSurface(tmpSurface);
	TTF_CloseFont(font);
	//std::cout << "STWORZONO TEXTURE Z NAPISEM: " << text << std::endl;

	return tex;
}
