﻿#include "Gra.h"

using namespace std;

Game* game = nullptr;

const int gameWidth = 1280; // <--- To zmieniamy
const int gameHight = gameWidth * 9 / 16;

const int FPS = 60;
const int frameDelay = 1000 / FPS;

int main(int argc, char * args[])
{
	Uint32 frameStart;
	int frameTime;

	game = new Game();
	game->init("DOTK", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, gameWidth, gameHight, 0);

	while (game->running())
	{
		frameStart = SDL_GetTicks();

		game->handleEvents();
		game->update();
		game->render();

		frameTime = SDL_GetTicks() - frameStart;
		if (frameTime < frameDelay)SDL_Delay(frameDelay - frameTime);

	}
	game->clean();
	return 0;
}

