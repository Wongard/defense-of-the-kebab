#pragma once

#include "Gra.h"

class TextureManager
{
public:
	static SDL_Texture* LoadTexture(const char* fName);
	static void Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);
	static SDL_Texture* FontTexture(const char* text, const char* fName, int fSize);
};

