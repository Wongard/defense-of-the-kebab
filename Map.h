#pragma once

#include "Gra.h"
#include "SWektor.h"
#include "TextureManager.h"


//odpowiada za mape do gry

class Map
{
public:
	Map(int w, int h);
	~Map();

	void loadMap();
	void drawMap();
	void saveMap();
	void changeMap(int key, SWektor<int,2> mouseXY);

private:

	SDL_Rect src, dest;
	SDL_Rect iSrc, iDest;


	SDL_Texture* grass1;
	SDL_Texture* grass2;
	SDL_Texture* grass3;
	SDL_Texture* roadLtoR;
	SDL_Texture* roadUtoD;
	SDL_Texture* roadLtoD;
	SDL_Texture* roadUtoL;
	SDL_Texture* roadRtoD;
	SDL_Texture* roadUtoR;
	SDL_Texture* water;

	int** mapa;
	int mapWidth, mapHeight, cellWidth, cellHeight;
};

