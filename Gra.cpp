#include "Gra.h"

using namespace std;

int stage = 1, mouseX, mouseY, keyPressed = 1;
bool isPressed = 0;
enum stage { mapMaker, game };

Map* mapa = nullptr;

SDL_Renderer* Game::renderer = NULL;

void Game::init(const char* name, int xpos, int ypos, int width, int height, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		//PODSTAWOWY INIT
		window = SDL_CreateWindow(name, xpos, ypos, width, height, fullscreen);
		renderer = SDL_CreateRenderer(window, -1, 0);
		isRunning = 1;

		//INIT OBIEKTOW GRY
		mapa = new Map(width, height);
		mapa->loadMap();

		stage = mapMaker;
	}
	else isRunning = 0;
}

void Game::handleEvents()
{
	SDL_Event event;
	SDL_PollEvent(&event);
	SDL_GetMouseState(&mouseX, &mouseY);
	switch (event.type)
	{
	case SDL_QUIT:
		isRunning = 0;
		break;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym)
		{
		case SDLK_ESCAPE:
			isRunning = 0;
			break;
		case SDLK_1:
			if (stage == 0) keyPressed = 1;
			break;
		case SDLK_2:
			if (stage == 0) keyPressed = 2;
			break;
		case SDLK_3:
			if (stage == 0) keyPressed = 3;
			break;
		case SDLK_4:
			if (stage == 0) keyPressed = 4;
			break;
		case SDLK_5:
			if (stage == 0) keyPressed = 5;
			break;
		case SDLK_6:
			if (stage == 0) keyPressed = 6;
			break;
		case SDLK_7:
			if (stage == 0) keyPressed = 7;
			break;
		case SDLK_8:
			if (stage == 0) keyPressed = 8;
			break;
		case SDLK_9:
			if (stage == 0) keyPressed = 9;
			break;
		case SDLK_0:
			if (stage == 0) keyPressed = 0;
			break;
		}
		isPressed = 0;
		break;
	case SDL_MOUSEBUTTONDOWN:
		isPressed = 1;
		//interfejs->interfaceClick(x, y);
		break;
	case SDL_MOUSEBUTTONUP:
		isPressed = 0;
		break;
	}
}
void Game::update()
{
	SDL_GetMouseState(&mouseXY(0), &mouseXY(1));
	if (isPressed == 1)
	{
		if ((stage == mapMaker)&&(czas%1==0)) mapa->changeMap(keyPressed,mouseXY);
	}
	czas++;
	if (czas >= 60)czas = 1;
}

void Game::render()
{
	SDL_RenderClear(renderer);

	mapa->drawMap();

	SDL_RenderPresent(renderer);
}

void Game::clean()
{
	if (stage == mapMaker) mapa -> saveMap();

	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	TTF_Quit;
	SDL_Quit();
	cout << "Pani wysprzatala ladnie" << endl;
}
