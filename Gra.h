#pragma once
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include <iostream>
#include "Map.h"
#include "SWektor.h"

class Game
{
public:
	void init(const char * name, int xpos,int ypos,int width,int height, bool fullscreen);
	void update();
	void render();
	void handleEvents();
	void clean();

	bool running() { return isRunning; } //Why are your runnin'?

	static SDL_Renderer* renderer;
private:
	bool isRunning;
	int czas; // Do wywalenia / klasa Czas?
	SDL_Window* window;
	SWektor <int, 2> mouseXY;
};