#ifndef SWEKTOR_HH
#define SWEKTOR_HH
#include <iostream>

template <typename STyp, int SWymiar>
class SWektor {
public:
	SWektor() { for (STyp& Wsp : tabW) Wsp = 0; }

	STyp operator  ()(int wiersz) const { return tabW[wiersz]; }
	STyp& operator ()(int wiersz)       { return tabW[wiersz]; }

	SWektor <STyp, SWymiar> operator - (const SWektor<STyp, SWymiar>& Odej);
	SWektor <STyp, SWymiar> operator + (const SWektor<STyp, SWymiar>& Dod);
	SWektor <STyp, SWymiar> operator * (const SWektor<STyp, SWymiar>& Wek2);
	SWektor <STyp, SWymiar> operator * (double mnoznik) const;
	SWektor <STyp, SWymiar> operator / (double dzielnik) const;
private:
	STyp tabW[SWymiar];
};


template<typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SWektor<STyp, SWymiar>::operator-(const SWektor<STyp, SWymiar>& Odej)
{
	SWektor<STyp, SWymiar> Wynik;
	for (int i = 0; i < SWymiar; i++) Wynik(i) = (*this)(i) - Odej(i);
	return Wynik;
}

template<typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SWektor<STyp, SWymiar>::operator+(const SWektor<STyp, SWymiar>& Dod)
{
	SWektor<STyp, SWymiar> Wynik;
	for (int i = 0; i < SWymiar; i++) Wynik(i) = (*this)(i) + Dod(i);
	return Wynik;
}

template<typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SWektor<STyp, SWymiar>::operator*(double mnoznik) const
{
	SWektor<STyp, SWymiar> Wynik;
	for (int i = 0; i < SWymiar; i++) Wynik(i) = (*this)(i) * mnoznik;
	return Wynik;
}

template<typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SWektor<STyp, SWymiar>::operator*(const SWektor<STyp, SWymiar>& Wek2)
{
	SWektor<STyp, SWymiar> Wynik;
	for (int i = 0; i < SWymiar; i++) Wynik(i) = (*this)(i) * Wek2(i);
	return Wynik;
}

template<typename STyp, int SWymiar>
SWektor<STyp, SWymiar> SWektor<STyp, SWymiar>::operator/(double dzielnik) const
{
	SWektor<STyp, SWymiar> Wynik;
	for (int i = 0; i < SWymiar; i++) Wynik(i) = (*this)(i) * dzielnik;
	return Wynik;
}
/*
 * Wypisuje wektor w postaci:
   a1
   a2
   ...
   an
 */
template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream& Strm, const SWektor<STyp, SWymiar>& W)
{
	for (unsigned int Ind = 0; Ind < SWymiar; ++Ind)
	{
		Strm << " " << W(Ind);
	}

	return Strm;
}
/*
 * Wczytuje Wektor od uzytkownika
 */
template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream& Strm, SWektor<STyp, SWymiar>& W)
{
	for (int i = 0; i < SWymiar; i++)
		Strm >> W(i);

	return Strm;
}
#endif