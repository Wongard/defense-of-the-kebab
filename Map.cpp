#include "Map.h"
#include <fstream>
#include <ctime>
#include <cstdlib>

const int cols = 25; // kolumny
const int rows = 20; // wiersze
int losujLiczbe(const unsigned int x,const unsigned int y);


Map::Map(int w, int h)
{
	SWektor <double, 2> skala; 
	skala(0) = 0.8; skala(1) = 1; // Przedzia� 0-1 : jak duza czesc gry zajmuje mapa (szerokosc,wysokosc)

	mapWidth	= static_cast<int>(skala(0) * w);
	mapHeight	= static_cast<int>(skala(1) * h);
	cellWidth	= static_cast<int>(mapWidth / cols);
	cellHeight	= static_cast<int>(mapHeight / rows);

	mapa = new int* [rows];
	for (int i = 0; i < rows; i++) mapa[i] = new int[cols];

	//<<< *** �adowanie Textur *** >>>                           //TypeOfEntity
	grass1 	 =	TextureManager::LoadTexture("image/grass1.bmp"); //1
	grass2	 =	TextureManager::LoadTexture("image/grass2.bmp"); //2
	grass3	 =	TextureManager::LoadTexture("image/grass3.bmp"); //3
	roadLtoR =	TextureManager::LoadTexture("image/roadF.bmp");  //11
	roadUtoD =	TextureManager::LoadTexture("image/roadUtoD.bmp"); //12
	roadLtoD =	TextureManager::LoadTexture("image/roadLtoD.bmp"); //13
	roadUtoL =	TextureManager::LoadTexture("image/roadUtoL.bmp"); //14
	roadRtoD =	TextureManager::LoadTexture("image/roadRtoD.bmp"); //15
	roadUtoR =	TextureManager::LoadTexture("image/roadUtoR.bmp"); //16
	water	 =	TextureManager::LoadTexture("image/water.bmp");

	// <<<*** Zmienne potrzebne do umieszczanie kafelek na mapie ** >>>
	src.x = src.y = dest.x = dest.y = 0;
	dest.w = src.w = cellWidth;
	dest.h = src.h = cellHeight;
}
void Map::loadMap()
{
	std::fstream plik;
	plik.open("mapa.dat", std::ios::in);
	if (plik.good())
	{
		int liczba,r=0,c=0;
		while(plik>>liczba)
		{	
			mapa[r][c] = liczba;
			std::cout << "L:" << liczba << std::endl;
			c++;
			if (c >= 25){ r++; c = 0; }
		}
	}
	plik.close();
}
void Map::drawMap()
{
	int type = 0;

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			type = mapa[i][j];
			//std::cout << type << std::endl;
			dest.x = j * cellWidth;
			dest.y = i * cellHeight;

			switch (type)
			{
			case 1:
				TextureManager::Draw(grass1, src, dest);
				break;
			case 2:
				TextureManager::Draw(grass2, src, dest);
				break;
			case 3:
				TextureManager::Draw(grass3, src, dest);
				break;
			case 11:
				TextureManager::Draw(roadLtoR, src, dest);
				break;
			case 12:
				TextureManager::Draw(roadUtoD, src, dest);
				break;
			case 13:
				TextureManager::Draw(roadLtoD, src, dest);
				break;
			case 14:
				TextureManager::Draw(roadUtoL, src, dest);
				break;
			case 15:
				TextureManager::Draw(roadRtoD, src, dest);
				break;
			case 16:
				TextureManager::Draw(roadUtoR, src, dest);
				break;
			case 20:
				TextureManager::Draw(water, src, dest);
				break;
			default:
				break;
			}
		}
	}

}

void Map::changeMap(int key, SWektor<int,2>mouseXY) //TODO: Cos tu polepszyc te % imo
{
	int typeOfEnv=0, randomNumber;
	int cellX = mouseXY(0) / cellWidth, cellY = mouseXY(1) / cellHeight;
	
	randomNumber = losujLiczbe(1, 100);
	switch (key)
	{
		case 1: //grass
			
			std::cout << "RandomNumberrrr: " << randomNumber << std::endl;
			std::cout << "KEY:" << key << std::endl;
			if		(randomNumber < 10)	typeOfEnv = 1; //10% na kwiatek
			else if (randomNumber > 90)	typeOfEnv = 3; //10% na salata kwiatek xD?
			else						typeOfEnv = 2; //80% na trawe 
			break;
		case 2: //road
			typeOfEnv = 11;
			break;
	}
	mapa[cellY][cellX] = typeOfEnv;
}

void Map::saveMap()
{
	std::fstream zapis;
	zapis.open("mapa.dat", std::ios::out);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++) zapis << mapa[i][j]<<" ";
}

int losujLiczbe(const unsigned int zakresDolny, const unsigned int zakresGorny)
{
	Uint32 duzaLicza;
	duzaLicza = SDL_GetTicks();
	srand(duzaLicza);
	return rand() % (zakresGorny - zakresDolny) + zakresDolny;
}